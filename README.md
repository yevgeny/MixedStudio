#MixedStudio
1. MixedStudio包含成员
-------------
>BrushSharp（图像处理软件，2013）

>AwakenJadeite（三维综合软件，2014）

>GraphicsMotor（图形编程软件，2014）

>Beetle（数据处理软件，2015）

2. 目前的版本
-------------
>Hydrogen（正在研发）

3. 命名规则
-------------
>[个人标识/机构名]+成员名+版本名+[性质]+更新号
    标有中括号的为可选项 版本第一次推出不加更新号
    例如：BrushSharp Hydrogen Academic Edition

>Parasites是官方的标识，其他个人或机构不得使用

>自2015年10月24日起Liu（不区分大小写）也作为官方标识，其他个人或机构不得使用

>版本名为元素周期顺序中化学元素的英文单词 何时启用下一个版本名由官方决定