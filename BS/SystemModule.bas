Attribute VB_Name = "SystemModule"

Sub layout(obj As Object)
    Dim i As Integer, j As Integer
    obj.AutoRedraw = True
    For i = 120 To obj.Width Step 120
        For j = 595 To obj.Height Step 120
            obj.PSet (i, j), RGB(88, 88, 88)
        Next j
    Next i
End Sub

Sub windowing(obj As PictureBox, Optional caption As String = "", Optional Enabled As Boolean = True, Optional lay As Boolean = False)
    Dim i As Integer, j As Integer
    Dim color As Long, r As Long, g As Long, b As Long
    obj.Visible = True
    obj.AutoRedraw = True
    obj.BorderStyle = 0
    obj.BackColor = RGB(44, 44, 44)
    If lay = True Then
        layout obj
    End If
    If Enabled = True Then
        frmimagebank.Picture1.AutoRedraw = True
        For i = 0 To (obj.Width - 25) Step 15
            For j = 0 To 225 Step 15
                k = (frmimagebank.Picture1.Point(i Mod (frmimagebank.Picture1.Width - 30), j Mod (frmimagebank.Picture1.Height - 30))) Mod 256
                w = (k / 255) * (i / (obj.Width - 25))
                obj.Line (i, j)-(i + 15, j + 15), RGB(88 * (1 - w) + 195 * w, 88 * (1 - w) + 195 * w, 88 * (1 - w) + 195 * w), BF
            Next j
        Next i
        obj.Line (0, 0)-(obj.Width - 10, obj.Height - 10), RGB(88, 88, 88), B
        obj.CurrentX = 15
        obj.CurrentY = 30
        obj.ForeColor = &H8000000B
        obj.Print " " & caption
        obj.Line (80, obj.Height - 90)-(0, obj.Height), obj.BackColor, BF
        obj.Line (obj.Width, obj.Height)-(obj.Width - 90, obj.Height - 90), obj.BackColor, BF
        obj.Circle (80, obj.Height - 90), 80, RGB(88, 88, 88), pi, pi * 1.6
        obj.Circle (obj.Width - 90, obj.Height - 90), 80, RGB(88, 88, 88), pi * 1.5, 2 * pi
    Else
        obj.Line (0, 0)-(obj.Width - 10, 240), &H80000006, BF
        obj.Line (0, 0)-(obj.Width - 10, obj.Height - 10), &H80000006, B
        obj.CurrentX = 15
        obj.CurrentY = 30
        obj.ForeColor = &H8000000C
        obj.Print " " & caption
        obj.Line (80, obj.Height - 90)-(0, obj.Height), obj.BackColor, BF
        obj.Line (obj.Width, obj.Height)-(obj.Width - 90, obj.Height - 90), obj.BackColor, BF
        obj.Circle (80, obj.Height - 90), 80, &H80000006, pi, pi * 1.6
        obj.Circle (obj.Width - 90, obj.Height - 90), 80, &H80000006, pi * 1.5, 2 * pi
    End If
    For i = 0 To (obj.Width - 25) Step 15
        For j = 0 To 100 Step 15
            color = obj.Point(i, j)
            r = color Mod 256
            g = (color Mod 65536) \ 256
            b = color \ 65536
            obj.Line (i, j)-(i + 3, j + 3), RGB(100 - j + r, 100 - j + g, 100 - j + b), BF
        Next j
    Next i
    For i = 1 To 114
        obj.Circle (80, 80), 80 + i, obj.BackColor, pi * 0.5, pi
        obj.Circle (obj.Width - 90, 80), 80 + i, obj.BackColor, 0, pi * 0.5
    Next i
End Sub

