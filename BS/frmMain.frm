VERSION 5.00
Begin VB.Form frmmain 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BrushSharp"
   ClientHeight    =   9000
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   12000
   ControlBox      =   0   'False
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.PictureBox listc 
      Height          =   375
      Index           =   0
      Left            =   5160
      ScaleHeight     =   315
      ScaleWidth      =   1515
      TabIndex        =   4
      Top             =   5520
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.PictureBox listb 
      Height          =   375
      Index           =   0
      Left            =   2880
      ScaleHeight     =   315
      ScaleWidth      =   1515
      TabIndex        =   3
      Top             =   5400
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.PictureBox Picture 
      Height          =   3735
      Index           =   0
      Left            =   4080
      ScaleHeight     =   3675
      ScaleWidth      =   2835
      TabIndex        =   2
      Top             =   840
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.PictureBox box 
      Height          =   3735
      Index           =   0
      Left            =   960
      ScaleHeight     =   3675
      ScaleWidth      =   2955
      TabIndex        =   1
      Top             =   840
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.PictureBox lista 
      Height          =   375
      Index           =   0
      Left            =   840
      ScaleHeight     =   315
      ScaleWidth      =   1515
      TabIndex        =   0
      Top             =   5520
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Shape Shape 
      BorderColor     =   &H00808080&
      Height          =   2175
      Index           =   0
      Left            =   7200
      Top             =   960
      Visible         =   0   'False
      Width           =   2055
   End
End
Attribute VB_Name = "frmmain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Dim boxflag As Integer, boxflag2 As Integer, boxl As Integer, boxt As Integer
Dim i As Double, j As Double, k As Double, w As Double
Dim color As Long, r As Long, g As Long, b As Long
Dim moj As Long
Dim cx As Single, cy As Single
Dim boxv(boxn) As Boolean
Private Sub lista_Click(Index As Integer)
    Select Case Index
        Case 1
            Me.WindowState = 1
        Case 2
            Me.WindowState = Abs(Me.WindowState - 2)
            If Me.WindowState = 0 Then
                Me.Left = (Screen.Width - Me.Width) / 2
                Me.Top = (Screen.Height - Me.Height) / 2
            End If
        Case 3
            End
    End Select
End Sub

Private Sub box_MouseDown(Index As Integer, button As Integer, Shift As Integer, X As Single, Y As Single)
    listhide
    If button = 1 Then
        If Y < 240 Then
            boxflag = Index
            For i = 1 To boxn                         '**
                box(i).Visible = False
                If boxv(i) = True Then
                    Shape(i).Visible = True
                End If
                Shape(i).Left = box(i).Left
                Shape(i).Top = box(i).Top
                Shape(i).Width = box(i).Width
                Shape(i).Height = box(i).Height
            Next i
            boxl = X
            boxt = Y
            box(Index).ZOrder
            layout Me
        End If
   End If
   If button = 2 Then
        If listb(1).Visible = False Then
            listb(1).Visible = True
            listb(2).Visible = True
            listb(3).Visible = True
            cx = X + box(Index).Left
            cy = Y + box(Index).Top
            listload 0, cx, cy
            listb(1).ZOrder
            listb(2).ZOrder
            listb(3).ZOrder
            boxflag2 = Index
        Else
            listhide
        End If
    End If
End Sub

Private Sub Form_Click()
    Form_Resize
    If boxflag <> -1 Then
        box(boxflag).Left = Shape(boxflag).Left
        box(boxflag).Top = Shape(boxflag).Top
        For i = 1 To boxn                               '**
            If boxv(i) = True Then
                box(i).Visible = True
            End If
            Shape(i).Visible = False
        Next i
        boxflag = -1
    End If
End Sub

Private Sub Form_Load()
    Dim sy As Long
    Dim newsy As Long
    Const GWL_STYLE = -16
    Const WS_CAPTION = &HC00000
    Const WS_BORDER = &H800000
    sy = GetWindowLong(Me.hwnd, GWL_STYLE)
    newsy = SetWindowLong(Me.hwnd, GWL_STYLE, sy - WS_CAPTION - WS_BORDER)
    Me.AutoRedraw = True
    boxflag = -1
    listloadf
    listload
    For i = 1 To boxn                                  '**
        Load Shape(i)
        Load box(i)
        boxv(i) = True
    Next i
    windowing box(1), "General", False
    windowing box(2), "Color"
    windowing box(3), "Tool"
    wheel box(2), 1400, 1400
    block box(2), 1400, 1400
    at box(2), 1400, 1400
    bar box(2), 300, 2600, False, 2200 / 15
    atcolor box(2), 2500, 2875, RGB(200, 200, 0), 255
End Sub

Private Sub Form_MouseDown(button As Integer, Shift As Integer, X As Single, Y As Single)
    listhide
    listload
    If Y < 240 Then
        If X > Me.Width - 240 Then
            If lista(1).Visible = False Then
                lista(1).Visible = True
                lista(2).Visible = True
                lista(3).Visible = True
                listreload
            Else
                listhide
            End If
        End If
    End If
    If X < Me.Width - 240 Or Y > 240 Then
        listhide
    End If
End Sub

Private Sub Form_MouseMove(button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 240 And Me.WindowState = 0 Then
        Static FormX%, FormY%
        If button = 1 Then
            Me.Move Me.Left - FormX + X, Me.Top - FormY + Y
        ElseIf button = 0 Then
            FormX = X
            FormY = Y
        End If
    End If
    cshow
    If boxflag <> -1 Then
        Shape(boxflag).Left = X - boxl
        Shape(boxflag).Top = Y - boxt
        Shape(boxflag).Left = Shape(boxflag).Left - (Shape(boxflag).Left Mod 120)
        Shape(boxflag).Top = Shape(boxflag).Top - (Shape(boxflag).Top Mod 120)
    End If
End Sub

Private Sub Form_Resize()
    Me.Cls
    frmimagebank.Picture1.AutoRedraw = True
    frmimagebank.Picture2.AutoRedraw = True
    Me.BackColor = RGB(44, 44, 44)
    For i = 0 To (Me.Width - 25) Step 15
        For j = 0 To 225 Step 15
            k = (frmimagebank.Picture1.Point(i Mod (frmimagebank.Picture1.Width - 30), j Mod (frmimagebank.Picture1.Height - 30))) Mod 256
            w = (k / 255) * (i / (Me.Width - 25))
            Me.Line (i, j)-(i + 15, j + 15), RGB(88 * (1 - w) + 195 * w, 88 * (1 - w) + 195 * w, 88 * (1 - w) + 195 * w), BF
        Next j
    Next i
    Me.Line (Me.Width - 240, 0)-(Me.Width, 240), RGB(88, 88, 88), BF
    For i = 0 To 240 - 25 Step 15
        For j = 0 To 225 Step 15
            Me.Line (i + Me.Width - 240, j)-(i + 15 + Me.Width - 240, j + 15), frmimagebank.Picture2.Point(i, j), BF
        Next j
    Next i
    Me.Line (0, 0)-(Me.Width - 10, Me.Height - 10), RGB(88, 88, 88), B
    For i = 0 To (Me.Width - 26) Step 15
        For j = 0 To 100 Step 15
            color = Me.Point(i, j)
            r = color Mod 256
            g = (color Mod 65536) \ 256
            b = color \ 65536
            Me.Line (i, j)-(i + 3, j + 3), RGB(100 - j + r, 100 - j + g, 100 - j + b), BF
        Next j
    Next i
    Me.CurrentX = 15
    Me.CurrentY = 30
    Me.ForeColor = &H8000000B
    Me.Print " ParasitesStudio BrushSharp Hydrogen Academic Edition"
    listload
    frmmain.Line (0, 505)-(frmmain.Width - 10, 505), RGB(195, 195, 195)
End Sub

Private Sub lista_MouseMove(Index As Integer, button As Integer, Shift As Integer, X As Single, Y As Single)
    listload Index
End Sub

Private Sub listb_Click(Index As Integer)
     Select Case Index
        Case 1
            box(boxflag2).Visible = False
            boxv(boxflag2) = False
            listb(1).Visible = False
            listb(2).Visible = False
            listb(3).Visible = False
    End Select
End Sub

Private Sub listb_MouseMove(Index As Integer, button As Integer, Shift As Integer, X As Single, Y As Single)
    listload Index, cx, cy
End Sub


Private Sub listc_MouseMove(Index As Integer, button As Integer, Shift As Integer, X As Single, Y As Single)
    i = Index / 100
    cshow
    Select Case i
            Case 1
                list frmmain.listc(i * 100), "File", 8, True, False
            Case 2
                list frmmain.listc(i * 100), "Edit", 8, True, False
            Case 3
                list frmmain.listc(i * 100), "Insert", 8, True, False
            Case 4
                list frmmain.listc(i * 100), "Bitmap", 8, True, False
            Case 5
                list frmmain.listc(i * 100), "Window", 8, True, False
            Case 6
                list frmmain.listc(i * 100), "Help", 8, True, False
    End Select
End Sub

