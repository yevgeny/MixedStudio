Attribute VB_Name = "ModuleList"
Sub list(obj As PictureBox, Optional caption As String = "", Optional longer As Integer = 6, Optional Index As Boolean = False, Optional line As Boolean = True)
    obj.AutoRedraw = True
    obj.BorderStyle = 0
    obj.Width = 90 * (longer + 4)
    obj.Height = 250
    obj.BackColor = RGB(44, 44, 44)
    obj.ForeColor = RGB(195, 195, 195)
    If Index = True Then
        obj.Line (0, 0)-(obj.Width - 10, 240), RGB(88, 88, 88), BF
    End If
    If line = True Then
        obj.Line (0, 0)-(obj.Width - 10, 240), RGB(88, 88, 88), B
    Else
        obj.Line (15, 30)-(15, 210), RGB(195, 195, 195)
    End If
    obj.CurrentX = 15
    obj.CurrentY = 30
    obj.Print "  " + caption
End Sub

Sub listload(Optional m As Integer = 0, Optional X As Single = 0, Optional Y As Single = 0)
    For i = 1 To lisan
        frmmain.lista(i).Cls
    Next i
    For i = 1 To lisbn
        frmmain.listb(i).Cls
    Next i
    For i = 1 To 3
        Select Case i
            Case 1
                list frmmain.lista(i), "Minimized", 15
                list frmmain.listb(i), "Hide", 15
            Case 2
                list frmmain.lista(i), "Normal/Maximized", 15
                list frmmain.listb(i), "Refresh", 15
            Case 3
                list frmmain.lista(i), "Exit", 15
                list frmmain.listb(i), "Properties", 15
        End Select
        frmmain.lista(i).Left = frmmain.Width - frmmain.lista(i).Width
        frmmain.lista(i).Top = 240 * i
        frmmain.listb(i).Left = X
        frmmain.listb(i).Top = Y + 240 * i - 240
    Next i
    Select Case m
        Case 1
            list frmmain.lista(m), "Minimized", 15, True
            list frmmain.listb(m), "Hide", 15, True
        Case 2
            list frmmain.lista(m), "Normal/Maximized", 15, True
            list frmmain.listb(m), "Refresh", 15, True
        Case 3
            list frmmain.lista(m), "Exit", 15, True
            list frmmain.listb(m), "Properties", 15, True
    End Select
    cshow
End Sub
Sub listloadf()
    For i = 1 To lisan
        Load frmmain.lista(i)
    Next i
    For i = 1 To lisbn
        Load frmmain.listb(i)
    Next i
    For i = 1 To liscn
        Load frmmain.listc(i * 100)
        frmmain.listc(i * 100).Visible = True
    Next i
    cshow
End Sub
Sub cshow()
    For i = 1 To liscn
        Select Case i
            Case 1
                list frmmain.listc(i * 100), "File", 8, , False
            Case 2
                list frmmain.listc(i * 100), "Edit", 8, , False
            Case 3
                list frmmain.listc(i * 100), "Insert", 8, , False
            Case 4
                list frmmain.listc(i * 100), "Bitmap", 8, , False
            Case 5
                list frmmain.listc(i * 100), "Window", 8, , False
            Case 6
                list frmmain.listc(i * 100), "Help", 8, , False
        End Select
        frmmain.listc(i * 100).Left = (frmmain.listc(i * 100).Width - 15) * (i - 1) + 15
        frmmain.listc(i * 100).Top = 255
    Next i
End Sub
Sub listreload(Optional m As Integer = 0)
    Select Case m
        Case 0
            For i = 1 To lisan
                frmmain.lista(i).ZOrder
            Next i
        Case 1
            For i = 1 To lisbn
                frmmain.listb(i).ZOrder
            Next i
    End Select
End Sub
Sub listhide()
    For i = 1 To lisan
        frmmain.lista(i).Visible = False
    Next i
    For i = 1 To lisbn
        frmmain.listb(i).Visible = False
    Next i
End Sub
