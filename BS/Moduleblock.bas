Attribute VB_Name = "Moduleblock"
Public systemfont As Font

Public Type square
    Width As Long
    Height As Long
End Type

Public Type Font
    b As Boolean
    i As Boolean
    u As Boolean
    s As Boolean
    Size As Long
    name As String
End Type

Sub loadf()
systemfont.b = False
systemfont.i = False
systemfont.u = False
systemfont.s = False
systemfont.Size = 8
systemfont.name = "MS Sans Serif"
End Sub

Sub bar(obj As Object, Optional X As Long = 0, Optional Y As Long = 0, Optional color As Boolean = False, Optional w As Long = 255)
    obj.AutoRedraw = True
    If color = False Then
        For i = 0 To 255
            obj.Line (X + i * (w / 17), Y)-(X + i * (w / 17) + (w / 17), Y + 240), RGB(i, i, i), BF
        Next i
    Else
        For i = 0 To 128
            obj.Line (X + i * (w / 51), Y)-(X + i * (w / 51) + (w / 51), Y + 240), RGB(255, 255 * (i Mod 128) / 128, 0), BF
        Next i
        For i = 128 To 128 * 2
            obj.Line (X + i * (w / 51), Y)-(X + i * (w / 51) + (w / 51), Y + 240), RGB(255 - (255 * (i Mod 128) / 128), 255, 0), BF
        Next i
        For i = 128 * 2 To 128 * 3
            obj.Line (X + i * (w / 51), Y)-(X + i * (w / 51) + (w / 51), Y + 240), RGB(0, 255, 255 * (i Mod 128) / 128), BF
        Next i
        For i = 128 * 3 To 128 * 4
            obj.Line (X + i * (w / 51), Y)-(X + i * (w / 51) + (w / 51), Y + 240), RGB(0, 255 - (255 * (i Mod 128) / 128), 255), BF
        Next i
        For i = 128 * 4 To 128 * 5
            obj.Line (X + i * (w / 51), Y)-(X + i * (w / 51) + (w / 51), Y + 240), RGB(255 * (i Mod 128) / 128, 0, 255), BF
        Next i
        For i = 128 * 5 To 128 * 6 - 1
            obj.Line (X + i * (w / 51), Y)-(X + i * (w / 51) + (w / 51), Y + 240), RGB(255, 0, 255 - (255 * (i Mod 128) / 128)), BF
        Next i
    End If
End Sub

Sub wheel(frmme As Object, Optional X As Long = 0, Optional Y As Long = 0, Optional m As Long = 100)
    frmme.AutoRedraw = True
    frmme.DrawWidth = 2
    For i = 0 To 239 Step 0.5
        r = 0
        g = 0
        b = 0
        If (i <= 40) Then r = 255
        If (i >= 40) And (i <= 120) Then g = 255
        If (i >= 120) And (i <= 200) Then b = 255
        If (i >= 200) Then r = 255
        If (i <= 40) Then g = 255 * (i / 40)
        If (i >= 40) And (i <= 80) Then r = 255 - 255 * ((i - 40) / 40)
        If (i >= 80) And (i <= 120) Then b = 255 * ((i - 80) / 40)
        If (i >= 120) And (i <= 160) Then g = 255 - 255 * ((i - 120) / 40)
        If (i >= 160) And (i <= 200) Then r = 255 * ((i - 160) / 40)
        If (i >= 200) And (i <= 239) Then b = 255 - 255 * ((i - 200) / 40)
        frmme.Line (X, Y)-(X + 1100 * Cos((i - m) * (3.1415 / 120)), Y + 1100 * Sin((i - m) * (3.1415 / 120))), RGB(r, g, b)
    Next i
    For i = 0 To 850
        frmme.Circle (X, Y), i, frmme.BackColor
    Next i
End Sub
Sub block(mm As Object, Optional X As Long = 0, Optional Y As Long = 0, Optional r As Long = 255, Optional g As Long = 0, Optional b As Long = 0, Optional lim As Long = 9)
    mm.AutoRedraw = True
    For i = -32 To 32
        For j = -32 To 32
            mi = i
            mj = j
            mx = mi * lim * 2
            my = mj * lim * 2
            mr = r + ((255 - r) / 64) * (64 - (mi + 32))
            mg = g + ((255 - g) / 64) * (64 - (mi + 32))
            mb = b + ((255 - b) / 64) * (64 - (mi + 32))
            mr = mr - ((mr / 64) * (mj + 32))
            mg = mg - ((mg / 64) * (mj + 32))
            mb = mb - ((mb / 64) * (mj + 32))
            mm.Line (X + mx - lim, Y + my - lim)-(X + mx + lim, Y + my + lim), RGB(mr, mg, mb), BF
        Next j
    Next i
End Sub
Sub at(obj As Object, X As Long, Y As Long)
    obj.AutoRedraw = True
    obj.DrawWidth = 2
    obj.Circle (X, Y), 50, RGB(255, 255, 255) - obj.Point(X, Y)
    obj.DrawWidth = 1
End Sub
Sub button(obj As Object, myfont As Font, Optional X As Long = 0, Optional Y As Long = 0, Optional caption As String = "")

End Sub
Function reprint(caption As String, myfont As Font) As square
    frmback.Label.FontBold = myfont.b
    frmback.Label.FontItalic = myfont.i
    frmback.Label.FontName = myfont.name
    frmback.Label.FontStrikethru = myfont.s
    frmback.Label.FontSize = myfont.Size
    frmback.Label.FontUnderline = myfont.u
    frmback.Label.caption = caption
    frmback.Label.AutoSize = True
    reprint.Width = frmback.Label.Width
    reprint.Height = frmback.Label.Height
End Function

Sub atcolor(obj As Object, X As Long, Y As Long, color As Long, a As Long, Optional up As Boolean = False)
    obj.AutoRedraw = True
    obj.DrawWidth = 1.5
    frmback.Label.caption = a
    frmback.Label.AutoSize = True
    obj.ForeColor = RGB(132, 132, 132)
    If up = False Then
        For i = 75 To 0 Step -1
            obj.Circle (X, Y + i * 2), i, RGB(88 + i / 75 * 88, 88 + i / 75 * 88, 88 + i / 75 * 88)
        Next i
        
        For i = 0 To 45
            obj.Circle (X, Y + 150), i, color
        Next i
        obj.CurrentX = X - (frmback.Label.Width / 2) - 45
        obj.CurrentY = Y + (frmback.Label.Height / 2) + 130
    Else
        For i = 75 To 0 Step -1
            obj.Circle (X, Y - i * 2), i, RGB(88 + i / 75 * 88, 88 + i / 75 * 88, 88 + i / 75 * 88)
        Next i
        
        For i = 0 To 45
            obj.Circle (X, Y - 150), i, color
        Next i
        obj.CurrentX = X - (frmback.Label.Width / 2) - 45
        obj.CurrentY = Y - frmback.Label.Height - 230
    End If
    obj.Print a
    obj.DrawWidth = 1
End Sub

