VERSION 5.00
Begin VB.Form frmSplash 
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   7500
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7500
   Icon            =   "frmSplash.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7500
   ScaleWidth      =   7500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timershow 
      Interval        =   1
      Left            =   5640
      Top             =   1920
   End
   Begin VB.Timer Timerlong 
      Left            =   5160
      Top             =   1920
   End
   Begin VB.Timer Timer 
      Left            =   4680
      Top             =   1920
   End
   Begin VB.Label l 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "This computer program is protected by Berkeley Software Distribution."
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   4
      Left            =   1920
      TabIndex        =   3
      Top             =   7200
      Width           =   4905
   End
   Begin VB.Label l 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "member of Parasites MixedStudio"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Index           =   3
      Left            =   1440
      TabIndex        =   2
      Top             =   6960
      Width           =   2340
   End
   Begin VB.Label l 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Academic Edition"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Index           =   2
      Left            =   1080
      TabIndex        =   1
      Top             =   6600
      Width           =   2220
   End
   Begin VB.Label l 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Parasites Beetle"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Index           =   1
      Left            =   840
      TabIndex        =   0
      Top             =   6240
      Width           =   1995
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Private Declare Function SetLayeredWindowAttributes Lib "user32" (ByVal hwnd As Long, ByVal crKey As Long, ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long

Dim i As Long

Private Sub Form_Load()
    i = 0
    Me.BackColor = RGB(163, 73, 164)
    For i = 1 To 4
        l(i).ForeColor = RGB(163, 73, 164)
    Next i
    Load tablemain
End Sub

Private Sub Timer_Timer()
    tablemain.Show
    Unload Me
End Sub

Private Sub Timerlong_Timer()
    flag = 0
    k1 = (l(1).Left - 120) / (840 - 120)
    k2 = (l(2).Left - 120) / (1080 - 120)
    k3 = (l(3).Left - 120) / (1440 - 120)
    k4 = (l(4).Left - 120) / (1920 - 120)
    For i = 1 To 4
        If i = 1 Then k = k1
        If i = 2 Then k = k2
        If i = 3 Then k = k3
        If i = 4 Then k = k4
        If l(i).Left > 120 Then
            l(i).Left = l(i).Left - 15
            l(i).ForeColor = RGB(163 + (255 - 163) * (1 - k), 73 + (255 - 73) * (1 - k), 164 + (255 - 164) * (1 - k))
            flag = 1
        End If
    Next i
    If flag = 0 Then
        Timer.Interval = 2500
        Timerlong.Interval = 0
    End If
End Sub

Private Sub Timershow_Timer()
    i = i + 10
    SetWindowLong hwnd, (-20), &H80000
    SetLayeredWindowAttributes Me.hwnd, vbBlack, i, 2
    If i >= 250 Then
        SetLayeredWindowAttributes Me.hwnd, vbBlack, 255, 2
        Timerlong.Interval = 1
        Timershow.Interval = 0
    End If
End Sub
