VERSION 5.00
Begin VB.Form tablemain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Beetle"
   ClientHeight    =   5760
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8385
   Icon            =   "tablemain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   8385
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox myin 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   0
      Left            =   120
      ScaleHeight     =   495
      ScaleWidth      =   1215
      TabIndex        =   5
      Top             =   2520
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox mye 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   0
      Left            =   120
      ScaleHeight     =   495
      ScaleWidth      =   1215
      TabIndex        =   4
      Top             =   1920
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox myh 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   0
      Left            =   120
      ScaleHeight     =   495
      ScaleWidth      =   1215
      TabIndex        =   3
      Top             =   1320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox mysheet 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   0
      Left            =   120
      ScaleHeight     =   495
      ScaleWidth      =   1575
      TabIndex        =   2
      Top             =   720
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox tabletext 
      Appearance      =   0  'Flat
      BackColor       =   &H80000006&
      Height          =   270
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label mylabel 
      AutoSize        =   -1  'True
      Caption         =   "label"
      Height          =   180
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   450
   End
End
Attribute VB_Name = "tablemain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

Dim myx As Long, myy As Long
Dim textin() As mydata
Dim mys As Long
Dim texsh() As mysheet
Dim i As Double, j As Double, k As Double, w As Double
Dim color As Long, r As Long, g As Long, b As Long

Private Sub Form_Load()
    Dim sy As Long
    Dim newsy As Long
    Const GWL_STYLE = -16
    Const WS_CAPTION = &HC00000
    Const WS_BORDER = &H800000
    sy = GetWindowLong(Me.hwnd, GWL_STYLE)
    newsy = SetWindowLong(Me.hwnd, GWL_STYLE, sy - WS_CAPTION - WS_BORDER)
    Me.AutoRedraw = True
    myx = 20
    myy = 12
    ReDim textin(myx, myy) As mydata
    Me.AutoRedraw = True
    Me.BackColor = RGB(44, 44, 44)
    mylabel.FontSize = 12
    mylabel.Caption = ".dung"
    Load mysheet(1)
    mysheet(1).Visible = True
    setsheet mysheet(1), "100", 1
    Load mysheet(2)
    mysheet(2).Visible = True
    setsheet mysheet(2), "100"
    mysheet(2).Top = 2000
End Sub
Private Sub Form_Resize()
    Me.Cls
    frmimagebank.Picture1.AutoRedraw = True
    frmimagebank.Picture2.AutoRedraw = True
    Me.BackColor = RGB(44, 44, 44)
    For i = 0 To (Me.Width - 25) Step 15
        For j = 0 To 225 Step 15
            k = (frmimagebank.Picture1.Point(i Mod (frmimagebank.Picture1.Width - 30), j Mod (frmimagebank.Picture1.Height - 30))) Mod 256
            w = (k / 255) * (i / (Me.Width - 25))
            Me.Line (i, j)-(i + 15, j + 15), RGB(88 * (1 - w) + 195 * w, 88 * (1 - w) + 195 * w, 88 * (1 - w) + 195 * w), BF
        Next j
    Next i
    Me.Line (Me.Width - 240, 0)-(Me.Width, 240), RGB(88, 88, 88), BF
    For i = 0 To 240 - 25 Step 15
        For j = 0 To 225 Step 15
            Me.Line (i + Me.Width - 240, j)-(i + 15 + Me.Width - 240, j + 15), frmimagebank.Picture2.Point(i, j), BF
        Next j
    Next i
    Me.Line (0, 0)-(Me.Width - 10, Me.Height - 10), RGB(88, 88, 88), B
    For i = 0 To (Me.Width - 26) Step 15
        For j = 0 To 100 Step 15
            color = Me.Point(i, j)
            r = color Mod 256
            g = (color Mod 65536) \ 256
            b = color \ 65536
            Me.Line (i, j)-(i + 3, j + 3), RGB(100 - j + r, 100 - j + g, 100 - j + b), BF
        Next j
    Next i
    Me.CurrentX = 15
    Me.CurrentY = 30
    Me.ForeColor = &H8000000B
    Me.Print " Parasites Beetle Hydrogen Academic Edition"
End Sub
Private Sub Form_MouseMove(button As Integer, Shift As Integer, X As Single, Y As Single)
    If Y < 240 And Me.WindowState = 0 Then
        Static FormX%, FormY%
        If button = 1 Then
            Me.Move Me.Left - FormX + X, Me.Top - FormY + Y
        ElseIf button = 0 Then
            FormX = X
            FormY = Y
        End If
    End If
End Sub
